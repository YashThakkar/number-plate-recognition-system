# Main.py
import tkinter as tk
from tkinter import filedialog
from tkinter import *
import cv2
import numpy as np
from PIL import ImageTk, Image
import os

import DetectChars
import DetectPlates
import PossiblePlate
from DBClass import *

# module level variables ##########################################################################
SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

showSteps = False

file_path = ""
number = ""
instruction_lbl= None 

addFrame = None
backbtn = None

# global variables
name_var = None
address_e = None
dob_var = None
age_var = 0
address_e = None
gendervar = None
email_var = None
contact1_var = None
contact2_var = None
marital_var = None
vehicle_type_var = None
model_var = None
register_var = None
number_var = None
editnumber_var = None


# def setUpperFrame():


def setFilePath(filepath):
    global file_path 
    file_path = filepath
    # print("setter: ", file_path)
    callMainMethod()

def getFilePath():
    global file_path
    # print("getter: ", file_path)
    return file_path

def upload_image():
    try:
        file_path=filedialog.askopenfilename()
        # return file_path
        setFilePath(file_path)  
    except:
        pass

def writeToFile(data, filename):
        #convert binary to proper format and storing in given folder project folder
        with open(filename, 'wb') as file:
            file.write(data)
        return file

def callMainMethod():
    dbc = DBClass()
    dbc.creating_database()
    
    # global entery variales
    global number
    number = main()

    number_lbl=Label(displayFrame,background='#080F19',fg="#fff", text=number,highlightbackground="#fff", borderwidth=1, relief="solid", font=('arial',35,'bold'))
    number_lbl.place(x=1200,y=0)# number_lbl.place(x=1200,y=10)

    # print("number : ", number)
    maininfo = dbc.fetchData(number)
    upperFrame.place_forget()

    print("-----------------------------------------")
    # name_ans_lbl.config(text="timepassre")
    # print("-> maininfo = ",maininfo)

    # photoPath = 'tempimg/'+((str)(maininfo[0]))+'.jpg'
    # user_image = writeToFile(maininfo[14],photoPath)

    setFormUI(maininfo)

def add_data():
    upperFrame.place_forget()
    setAddFormUI()

def clearDisplayFrame():
    displayFrame.place_forget()
    upperFrame.place(x=0, y=150)

def setFormUI(maininfo):

    
    # f = Image.open('tempimg/'+((str)(maininfo[0]))+'.jpg')
    # # f.show()
    # img = f.resize((215,250),Image.ANTIALIAS)
    # userimg = ImageTk.PhotoImage(img)
    # selected_img = Label(top,width=215,height=250, image=userimg)
    # selected_img.place(x=1110,y=250)
    
    #---------------------------------------------

    # if(len(maininfo) > 0):
    #     Error_lbl = Label(top, text='Invalid Number PLate!', fg='RED', bg='#CDCDCD', font=('Raleway', 20, 'bold')).place(x=950, y=300)
    #     return 

    #-------- FORM ------
    displayFrame.place(x=0, y=0)

    title_lbl = Label(displayFrame, text='Number Plate Recognition System', fg='#fff', bg='#080F19', font=('Raleway', 25, 'underline')).place(x=550, y=10)

    upload=Button(displayFrame,text="Upload an image",cursor='hand2',command=upload_image,padx=9,pady=4)
    upload.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    upload.pack()
    upload.place(x=1100,y=600)

    backbtn=Button(displayFrame,text="Back",cursor='hand2',command=clearDisplayFrame,padx=10,pady=5)
    backbtn.configure(background='#364156',foreground='white',font=('arial',14))
    backbtn.place(x=120,y=10)

    name_lbl = Label(displayFrame, text='Name: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=150)
    name_ans_lbl = Label(displayFrame, text=maininfo[1], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=150, y=145)
    name_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=150, y=175)

    dob_lbl = Label(displayFrame, text='DOB: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=250)
    dob_ans_lbl = Label(displayFrame, text=maininfo[2], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=150, y=245)
    dob_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=150, y=275)

    age_lbl = Label(displayFrame, text='Age: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=350)
    age_ans_lbl = Label(displayFrame, text=maininfo[4], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=150, y=345)
    age_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=150, y=375)

    address_lbl = Label(displayFrame, text='Address: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=90, y=450)
    address_e = Text(displayFrame,width=20,height=5, bd=1, wrap=WORD, fg='#fff', bg='#080F19', font=('Raleway', 13))
    address_e.insert(END,maininfo[3])
    address_e.place(x=160, y=445)
    address_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=160, y=550)

    gender_lbl = Label(displayFrame, text='Gender: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=650)
    gender_ans_lbl = Label(displayFrame, text=maininfo[5], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=150, y=650)
    gender_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=150, y=680)

    email_lbl = Label(displayFrame, text='Email: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=150)
    email_ans_lbl = Label(displayFrame, text=maininfo[6], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=610, y=150)
    email_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=610, y=175)

    contact1_lbl = Label(displayFrame, text='Contact(1): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=250)
    contact1_ans_lbl = Label(displayFrame, text=maininfo[7], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=635, y=250)
    contact1_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=635, y=275)

    contact2_lbl = Label(displayFrame, text='Contact(2): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=350)
    contact2_ans_lbl = Label(displayFrame, text=maininfo[8], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=635, y=350)
    contact2_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=635, y=375)

    marital_status_lbl = Label(displayFrame, text='Marital Status: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=450)
    marital_status_ans_lbl = Label(displayFrame, text=maininfo[9], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=670, y=450)
    marital_status_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=670, y=475)

    vehicle_type_lbl = Label(displayFrame, text='Vehicle: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=550)
    vehicle_type_ans_lbl = Label(displayFrame, text=maininfo[10], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=635, y=550)
    vehicle_type_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=635, y=575)

    vehicle_model_lbl = Label(displayFrame, text='Model: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=650)
    vehicle_model_ans_lbl = Label(displayFrame, text=maininfo[11], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=635, y=650)
    vehicle_model_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=635, y=675)

    vehicle_register_lbl = Label(displayFrame, text='Registeration Date: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=950, y=150)
    vehicle_register_lbl = Label(displayFrame, text=maininfo[12], width=18, fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=1110, y=150)
    vehicle_register_line_frame = Frame(displayFrame, width=200,height=2,bg='#91C46B').place(x=1110, y=175)

def setgenderOption():
    if(gendervar.get() == 1):
        maleOption.config(fg="#91C46B")
        femaleOption.config(fg="#080F19")
        return "Male"
    elif(gendervar.get() == 2):
        femaleOption.config(fg="#91C46B")
        maleOption.config(fg="#080F19")
        return "Female"

# def clearAllFields():
#     print("Clear")
    # global name_ans_txt
    # name_ans_txt.delete(0,END)
    # dob_ans_txt.delete(0,END)
    # age_ans_txt.delete(0,END)
    # address_e.delete("1.0","end")
    # email_ans_txt.delete(0,END)
    # contact1_ans_lbl.delete(0,END)
    # contact2_ans_lbl.delete(0,END)
    # marital_status_ans_lbl.delete(0,END)
    # vehicle_type_ans_lbl.delete(0,END)
    # vehicle_model_ans_lbl.delete(0,END)
    # vehicle_register_lbl.delete(0,END)
    # vehicle_register_lbl.delete(0,END)


def data_submit():
    dbc = DBClass()

    name = name_var.get()
    dob = dob_var.get()
    age = (int)(age_var.get())
    addr = address_e.get(1.0,END)
    gender = setgenderOption()#gendervar.get()
    email = email_var.get()
    contact1 = contact1_var.get()
    contact2 = contact2_var.get()
    marital = marital_var.get()
    vehicle_type = vehicle_type_var.get()
    vehicle_model = model_var.get()
    vehicle_register = register_var.get()
    number = number_var.get()

    dbc.insert_data(name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, number)

def clearTopFrame():
    
    global addFrame
    addFrame.place_forget()
    upperFrame.place(x=0, y=150)

def clearEditFrame():
  # global addFrame
    editFrame.place_forget()
    editDialogFrame.place_forget()
    dialogAlert_lbl.place_forget()

    upperFrame.place(x=0, y=150)

    # global backbtn
    # backbtn.place_forget()

    # upperFrame = Frame(top, height=800, width=1920, bg='#080F19')
    # upperFrame.place(x=0, y=150)

    # instruction_lbl = Label(upperFrame, text='Please Upload image by clicking below button', fg='#ccc', bg='#080F19', font=('Raleway', 16))
    # instruction_lbl.place(x=570, y=150)

    # lowerImg = "img/lower.png"
    # lf = Image.open(lowerImg)
    # # img = lf.resize((1550,350),Image.ANTIALIAS)
    # limg = ImageTk.PhotoImage(lf)
    # lowerImg_lbl = Label(upperFrame,width=1550,height=350,bg='#080F19',image=limg)
    # lowerImg_lbl.place(x=0, y=400)

    # upload=Button(upperFrame,text="Upload an image",cursor='hand2',command=upload_image,padx=10,pady=5)
    # upload.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    # upload.pack()
    # upload.place(x=700,y=400)

    # add=Button(upperFrame,text="Add data",cursor='hand2',command=add_data,padx=10,pady=5)
    # add.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    # add.pack()
    # add.place(x=735,y=460)


def setAddFormUI():

    addFrame.place(x=0,y=0)
    # global addFrame;
    # addFrame = Frame(top, height=800, width=1920, bg='#080F19').place(x=0,y=0)
    title_lbl = Label(addFrame, text='Number Plate Recognition System', fg='#fff', bg='#080F19', font=('Raleway', 25, 'underline')).place(x=550, y=10)

    # backbtn=Button(top,text="Back",cursor='hand2',command=clearTopFrame,padx=10,pady=5)
    backbtn.configure(background='#364156', foreground='white',font=('arial',14))
    backbtn.place(x=120,y=10)


    # upload=Button(addFrame,text="Upload an image",cursor='hand2',command=upload_image,padx=10,pady=5)
    # upload.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    # upload.pack()
    # upload.place(x=1100,y=600)
    
    submit=Button(addFrame,text="Submit",cursor='hand2',command=data_submit,padx=10,pady=5)
    submit.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    submit.pack()
    submit.place(x=1100,y=500)

    name_lbl = Label(addFrame, text='Name: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=150)
    global name_var
    name_var = StringVar()
    name_ans_txt = Entry(addFrame, width=20,state=NORMAL,textvariable = name_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=150, y=145)
    name_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=150, y=175)

    dob_lbl = Label(addFrame, text='DOB: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=250)
    global dob_var
    dob_var = StringVar()
    dob_ans_txt = Entry(addFrame, width=20,state=NORMAL,textvariable = dob_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=150, y=245)
    dob_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=150, y=275)

    age_lbl = Label(addFrame, text='Age: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=350)
    global age_var
    age_var = IntVar()
    age_ans_txt = Entry(addFrame, width=20,state=NORMAL,textvariable = age_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    age_ans_txt.place(x=150, y=345)
    age_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=150, y=375)
    # age_ans_txt.delete(0,END)

    address_lbl = Label(addFrame, text='Address: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=90, y=450)
    global address_e
    address_e = Text(addFrame,width=20,height=5, bd=1, wrap=WORD, fg='#fff', bg='#080F19', font=('Raleway', 13))
    # address_e.insert(END,maininfo[3])
    address_e.place(x=160, y=445)
    address_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=160, y=550)

    global gendervar
    gendervar = IntVar()
    gender_lbl = Label(addFrame, text='Gender: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=650)
    maleOption = Radiobutton(addFrame, bg="#080F19", fg="#ccc",activebackground='#080F19',activeforeground='#91C46B', font=('Raleway', 13), text="Male", variable=gendervar, value=1, command=setgenderOption)
    femaleOption = Radiobutton(addFrame, bg="#080F19", fg="#ccc",activeforeground='#080F19', font=('Raleway', 13), text="Female", variable=gendervar, value=2, command=setgenderOption)
    maleOption.place(x=150, y=650)
    femaleOption.place(x=220, y=650)
    # gender_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=150, y=680)

    global email_var
    email_var = StringVar()
    email_lbl = Label(addFrame, text='Email: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=150)
    email_ans_txt = Entry(addFrame, width=20,state=NORMAL,textvariable = email_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=610, y=150)
    email_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=610, y=175)

    global contact1_var
    contact1_var = StringVar()
    contact1_lbl = Label(addFrame, text='Contact(1): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=250)
    contact1_ans_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = contact1_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=635, y=250)
    contact1_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=635, y=275)

    global contact2_var
    contact2_var = StringVar()
    contact2_lbl = Label(addFrame, text='Contact(2): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=350)
    contact2_ans_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = contact2_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=635, y=350)
    contact2_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=635, y=375)

    global marital_var
    marital_var = StringVar()
    marital_status_lbl = Label(addFrame, text='Marital Status: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=450)
    marital_status_ans_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = marital_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=670, y=450)
    marital_status_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=670, y=475)

    global vehicle_type_var
    vehicle_type_var = StringVar()
    vehicle_type_lbl = Label(addFrame, text='Vehicle: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=550)
    vehicle_type_ans_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = vehicle_type_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=635, y=550)
    vehicle_type_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=635, y=575)

    global model_var
    model_var = StringVar()
    vehicle_model_lbl = Label(addFrame, text='Model: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=650)
    vehicle_model_ans_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = model_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=635, y=650)
    vehicle_model_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=635, y=675)

    global register_var
    register_var = StringVar()
    vehicle_register_lbl = Label(addFrame, text='Registeration Date: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=950, y=150)
    vehicle_register_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = register_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=1110, y=150)
    vehicle_register_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=1110, y=175)

    global number_var
    number_var = StringVar()
    vehicle_register_lbl = Label(addFrame, text='Plate Number', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=950, y=250)
    vehicle_register_lbl = Entry(addFrame, width=20,state=NORMAL,textvariable = number_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=1110, y=250)
    vehicle_register_line_frame = Frame(addFrame, width=200,height=2,bg='#91C46B').place(x=1110, y=275)    

    # clearAllFields()
###################################################################################################
def main():

    plate_number = ""
    
    blnKNNTrainingSuccessful = DetectChars.loadKNNDataAndTrainKNN()         # attempt KNN training

    if blnKNNTrainingSuccessful == False:                               # if KNN training was not successful
        print("\nerror: KNN traning was not successful\n")  # show error message
        return                                                          # and exit program
    # end if

    print("getting filepath: ", getFilePath())

    imgOriginalScene  = cv2.imread(getFilePath())#("LicPlateImages/1.png")               # open image

    if imgOriginalScene is None:                            # if image was not read successfully
        print("\nerror: image not read from file \n\n")  # print error message to std out
        os.system("pause")                                  # pause so user can see error message
        return                                              # and exit program
    # end if

    listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginalScene)           # detect plates

    listOfPossiblePlates = DetectChars.detectCharsInPlates(listOfPossiblePlates)        # detect chars in plates

    cv2.imshow("imgOriginalScene", imgOriginalScene)            # show scene image

    if len(listOfPossiblePlates) == 0:                          # if no plates were found
        print("\nno license plates were detected\n")  # inform user no plates were found
    else:                                                       # else
                # if we get in here list of possible plates has at leat one plate

                # sort the list of possible plates in DESCENDING order (most number of chars to least number of chars)
        listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)

                # suppose the plate with the most recognized chars (the first plate in sorted by string length descending order) is the actual plate
        licPlate = listOfPossiblePlates[0]

        cv2.imshow("imgPlate", licPlate.imgPlate)           # show crop of plate and threshold of plate
        cv2.imshow("imgThresh", licPlate.imgThresh)

        if len(licPlate.strChars) == 0:                     # if no chars were found in the plate
            print("\nno characters were detected\n\n")  # show message
            return                                          # and exit program
        # end if

        drawRedRectangleAroundPlate(imgOriginalScene, licPlate)             # draw red rectangle around plate

        print("\nlicense plate read from image = " + licPlate.strChars + "\n")  # write license plate text to std out
        plate_number = licPlate.strChars
        print("----------------------------------------")

        writeLicensePlateCharsOnImage(imgOriginalScene, licPlate)           # write license plate text on the image

        cv2.imshow("imgOriginalScene", imgOriginalScene)                # re-show scene image

        cv2.imwrite("imgOriginalScene.png", imgOriginalScene)           # write image out to file

    # end if else

    # cv2.waitKey(0)                    # hold windows open until user presses a key

    # top.mainloop()
    
    return plate_number

# end main

###################################################################################################
def drawRedRectangleAroundPlate(imgOriginalScene, licPlate):

    p2fRectPoints = cv2.boxPoints(licPlate.rrLocationOfPlateInScene)            # get 4 vertices of rotated rect

    cv2.line(imgOriginalScene, tuple(p2fRectPoints[0]), tuple(p2fRectPoints[1]), SCALAR_RED, 2)         # draw 4 red lines
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[1]), tuple(p2fRectPoints[2]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[2]), tuple(p2fRectPoints[3]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[3]), tuple(p2fRectPoints[0]), SCALAR_RED, 2)
# end function

###################################################################################################
def writeLicensePlateCharsOnImage(imgOriginalScene, licPlate):
    ptCenterOfTextAreaX = 0                             # this will be the center of the area the text will be written to
    ptCenterOfTextAreaY = 0

    ptLowerLeftTextOriginX = 0                          # this will be the bottom left of the area that the text will be written to
    ptLowerLeftTextOriginY = 0

    sceneHeight, sceneWidth, sceneNumChannels = imgOriginalScene.shape
    plateHeight, plateWidth, plateNumChannels = licPlate.imgPlate.shape

    intFontFace = cv2.FONT_HERSHEY_SIMPLEX                      # choose a plain jane font
    fltFontScale = float(plateHeight) / 30.0                    # base font scale on height of plate area
    intFontThickness = int(round(fltFontScale * 1.5))           # base font thickness on font scale

    textSize, baseline = cv2.getTextSize(licPlate.strChars, intFontFace, fltFontScale, intFontThickness)        # call getTextSize

            # unpack roatated rect into center point, width and height, and angle
    ( (intPlateCenterX, intPlateCenterY), (intPlateWidth, intPlateHeight), fltCorrectionAngleInDeg ) = licPlate.rrLocationOfPlateInScene

    intPlateCenterX = int(intPlateCenterX)              # make sure center is an integer
    intPlateCenterY = int(intPlateCenterY)

    ptCenterOfTextAreaX = int(intPlateCenterX)         # the horizontal location of the text area is the same as the plate

    if intPlateCenterY < (sceneHeight * 0.75):                                                  # if the license plate is in the upper 3/4 of the image
        ptCenterOfTextAreaY = int(round(intPlateCenterY)) + int(round(plateHeight * 1.6))      # write the chars in below the plate
    else:                                                                                       # else if the license plate is in the lower 1/4 of the image
        ptCenterOfTextAreaY = int(round(intPlateCenterY)) - int(round(plateHeight * 1.6))      # write the chars in above the plate
    # end if

    textSizeWidth, textSizeHeight = textSize                # unpack text size width and height

    ptLowerLeftTextOriginX = int(ptCenterOfTextAreaX - (textSizeWidth / 2))           # calculate the lower left origin of the text area
    ptLowerLeftTextOriginY = int(ptCenterOfTextAreaY + (textSizeHeight / 2))          # based on the text area center, width, and height

            # write the text on the image
    cv2.putText(imgOriginalScene, licPlate.strChars, (ptLowerLeftTextOriginX, ptLowerLeftTextOriginY), intFontFace, fltFontScale, SCALAR_YELLOW, intFontThickness)
# end function

###################################################################################################

def adminLogin():
    loginFrame.place(x=0,y=150)
    loginBtn.place_forget()

def loginSubmit():
    username = username_var.get()
    password = password_var.get()
    dbc = DBClass()
    result = dbc.checkCredentials(username, password)
    if(result >0):
        logged_in = 1
        loginFrame.place_forget()
        add.place(x=735,y=460)
        edit.place(x=735,y=340)
        logout_submit.place(x=10,y=10)

def logoutSubmit():
    logged_in = 0
    add.place_forget()
    edit.place_forget()
    addFrame.place_forget()
    editFrame.place_forget()
    editDialogFrame.place_forget()
    dialogAlert_lbl.place_forget()
    logout_submit.place_forget()
    upperFrame.place(x=0, y=150)
    loginBtn.place(x=1350,y=10)

def login_back():
    loginFrame.place_forget()
    loginBtn.place(x=1350,y=10)

def setEditFormUI(maininfo):

    editFrame.place(x=0,y=0)
    # global addFrame;
    # addFrame = Frame(top, height=800, width=1920, bg='#080F19').place(x=0,y=0)
    title_lbl = Label(editFrame, text='Number Plate Recognition System', fg='#fff', bg='#080F19', font=('Raleway', 25, 'underline')).place(x=550, y=10)

    # editbackbtn=Button(top,text="Back",cursor='hand2',command=clearTopFrame,padx=10,pady=5)
    editbackbtn.configure(background='#364156', foreground='white',font=('arial',14))
    editbackbtn.place(x=120,y=10)


   
    submit=Button(editFrame,text="Confirm Edit",cursor='hand2',command=edit_data,padx=10,pady=5)
    submit.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    submit.pack()
    submit.place(x=1100,y=500)

    name_lbl = Label(editFrame, text='Name: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=150)
    global name_var
    name_var = StringVar()
    name_ans_txt = Entry(editFrame, width=20,state=NORMAL,textvariable = name_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    name_ans_txt.place(x=150, y=145)
    name_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=150, y=175)
    name_ans_txt.insert(0, maininfo[1])

    dob_lbl = Label(editFrame, text='DOB: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=250)
    global dob_var
    dob_var = StringVar()
    dob_ans_txt = Entry(editFrame, width=20,state=NORMAL,textvariable = dob_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    dob_ans_txt.place(x=150, y=245)
    dob_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=150, y=275)
    dob_ans_txt.insert(0, maininfo[2])

    age_lbl = Label(editFrame, text='Age: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=350)
    global age_var
    age_var = IntVar()
    age_ans_txt = Entry(editFrame, width=20,state=NORMAL,textvariable = age_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    age_ans_txt.place(x=150, y=345)
    age_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=150, y=375)
    age_ans_txt.delete(0,END)
    age_ans_txt.insert(0, str(maininfo[4]))

    address_lbl = Label(editFrame, text='Address: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=90, y=450)
    global address_e
    address_e = Text(editFrame,width=20,height=5, bd=1, wrap=WORD, fg='#fff', bg='#080F19', font=('Raleway', 13))
    address_e.place(x=160, y=445)
    address_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=160, y=550)
    address_e.insert(END,maininfo[3])

    global gendervar
    gendervar = IntVar()
    gender_lbl = Label(editFrame, text='Gender: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=80, y=650)
    maleOption = Radiobutton(editFrame, background="#080F19", fg="#ccc",activebackground='#91C46B',activeforeground='red', font=('Raleway', 13), text="Male", variable=gendervar, value=1, command=setgenderOption)
    femaleOption = Radiobutton(editFrame, background="#080F19", fg="#ccc",activebackground='#91C46B',activeforeground='red', font=('Raleway', 13), text="Female", variable=gendervar, value=2, command=setgenderOption)
    maleOption.place(x=150, y=650)
    femaleOption.place(x=220, y=650)
    if(maininfo[5] == "Female"):
        femaleOption.config(fg="#91C46B")
        gendervar.set(2)
    elif(maininfo[5] == "Male"):
        gendervar.set(1)
        maleOption.config(fg="#91C46B")
    # gender_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=150, y=680)

    global email_var
    email_var = StringVar()
    email_lbl = Label(editFrame, text='Email: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=150)
    email_ans_txt = Entry(editFrame, width=20,state=NORMAL,textvariable = email_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    email_ans_txt.place(x=610, y=150)
    email_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=610, y=175)
    email_ans_txt.insert(0, maininfo[6])

    global contact1_var
    contact1_var = StringVar()
    contact1_lbl = Label(editFrame, text='Contact(1): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=250)
    contact1_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = contact1_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    contact1_ans_lbl.place(x=635, y=250)
    contact1_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=635, y=275)
    contact1_ans_lbl.insert(0, maininfo[7])

    global contact2_var
    contact2_var = StringVar()
    contact2_lbl = Label(editFrame, text='Contact(2): ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=350)
    contact2_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = contact2_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    contact2_ans_lbl.place(x=635, y=350)
    contact2_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=635, y=375)
    contact2_ans_lbl.insert(0, maininfo[8])

    global marital_var
    marital_var = StringVar()
    marital_status_lbl = Label(editFrame, text='Marital Status: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=450)
    marital_status_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = marital_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    marital_status_ans_lbl.place(x=670, y=450)
    marital_status_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=670, y=475)
    marital_status_ans_lbl.insert(0, maininfo[9])

    global vehicle_type_var
    vehicle_type_var = StringVar()
    vehicle_type_lbl = Label(editFrame, text='Vehicle: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=550)
    vehicle_type_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = vehicle_type_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    vehicle_type_ans_lbl.place(x=635, y=550)
    vehicle_type_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=635, y=575)
    vehicle_type_ans_lbl.insert(0, maininfo[10])

    global model_var
    model_var = StringVar()
    vehicle_model_lbl = Label(editFrame, text='Model: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=550, y=650)
    vehicle_model_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = model_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    vehicle_model_ans_lbl.place(x=635, y=650)
    vehicle_model_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=635, y=675)
    vehicle_model_ans_lbl.insert(0, maininfo[11])

    global register_var
    register_var = StringVar()
    vehicle_register_lbl = Label(editFrame, text='Registeration Date: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=950, y=150)
    vehicle_register_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = register_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    vehicle_register_lbl.place(x=1110, y=150)
    vehicle_register_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=1110, y=175)
    vehicle_register_lbl.insert(0, maininfo[12])

    global number_var
    number_var = StringVar()
    vehicle_register_lbl = Label(editFrame, text='Plate Number', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=950, y=250)
    vehicle_register_ans_lbl = Entry(editFrame, width=20,state=NORMAL,textvariable = number_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12))
    vehicle_register_ans_lbl.place(x=1110, y=250)
    vehicle_register_line_frame = Frame(editFrame, width=200,height=2,bg='#91C46B').place(x=1110, y=275)
    vehicle_register_ans_lbl.insert(0, maininfo[13])
    vehicle_register_ans_lbl.config(state='disabled')

def openEditDialog():
    # editDialogFrame = Frame(upperFrame, height=300, width=500, bg='#14253E')
    editDialogFrame.place(x=550,y=150)

    closeDialog=Button(editDialogFrame,text="X",bg="#14253E", fg="#fff",font=('Raleway', 14, 'bold'), cursor='hand2',command=closeEditDialog,padx=10,pady=10)
    closeDialog.place(x=455,y=0)

    dialog_lbl = Label(editDialogFrame, text='Edit', fg='#fff', bg='#14253E', font=('Raleway', 13, 'bold')).place(x=240, y=10)

    global editnumber_var
    editnumber_var = StringVar()
    vehicle_register_lbl = Label(editDialogFrame, text='Plate Number', fg='#fff', bg='#14253E', font=('Raleway', 13)).place(x=80, y=150)
    vehicle_register_lbl = Entry(editDialogFrame, width=20,state=NORMAL,textvariable = editnumber_var, fg='#909394', bg='#14253E',bd=0, font=('Raleway', 12)).place(x=210, y=150)
    vehicle_register_line_frame = Frame(editDialogFrame, width=200,height=2,bg='#91C46B').place(x=210, y=175)

    editDialogNext=Button(editDialogFrame,text="Next",bg="#14253E", fg="#fff",font=('Raleway', 12, ), cursor='hand2',command=editDialogNextClick,padx=6,pady=5)
    editDialogNext.place(x=350,y=200)


def edit_data():
    dbc = DBClass()

    name = name_var.get()
    dob = dob_var.get()
    age = (int)(age_var.get())
    addr = address_e.get(1.0,END)
    gender = setgenderOption()#gendervar.get()
    email = email_var.get()
    contact1 = contact1_var.get()
    contact2 = contact2_var.get()
    marital = marital_var.get()
    vehicle_type = vehicle_type_var.get()
    vehicle_model = model_var.get()
    vehicle_register = register_var.get()
    number = number_var.get()

    #update called
    dbc.update_data(name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, number)

def editDialogNextClick():
    dbc = DBClass()

    # print("Next clicked!")
    global editnumber_var
    num = editnumber_var.get()
    maininfo = dbc.fetchData(num)
    # clearAllFields()
    print("maininfo : ",len(maininfo))
    if((len(maininfo)) > 0):
        dialogAlert_lbl.place_forget()
        upperFrame.place_forget()
        setEditFormUI(maininfo)
    else:
        dialogAlert_lbl.place(x=150, y=250)


def closeEditDialog():
    editDialogFrame.place_forget()
    dialogAlert_lbl.place_forget()

if __name__ == "__main__":
    top=tk.Tk()
    top.geometry('900x700')
    top.title('Number Plate Recognition')
    # top.iconphoto(True, PhotoImage(file="logo.png"))
    # img = ImageTk.PhotoImage(Image.open("logo.png"))
    top.configure(background='#080F19')

    logged_in = 0

    title_lbl = Label(top, text='Number Plate Recognition System', fg='#fff', bg='#080F19', font=('Raleway', 25, 'underline')).place(x=550, y=10)

    upperFrame = Frame(top, height=800, width=1920, bg='#080F19')
    upperFrame.place(x=0, y=150)

    instruction_lbl = Label(upperFrame, text='Please Upload image by clicking below button', fg='#ccc', bg='#080F19', font=('Raleway', 16))
    instruction_lbl.place(x=570, y=150)

    lowerImg = "img/lower.png"
    lf = Image.open(lowerImg)
    img = lf.resize((1550,350),Image.ANTIALIAS)
    limg = ImageTk.PhotoImage(img)
    lowerImg_lbl = Label(upperFrame,width=1550,height=350,bg='#080F19',image=limg)
    lowerImg_lbl.place(x=0, y=400)

    loginBtn=Button(top,text="Admin Login",cursor='hand2',command=adminLogin,padx=10,pady=5)
    loginBtn.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    loginBtn.place(x=1350,y=10)

    upload=Button(upperFrame,text="Upload an image",cursor='hand2',command=upload_image,padx=10,pady=5)
    upload.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    upload.pack()
    upload.place(x=700,y=400)

    add=Button(upperFrame,text="Add data",cursor='hand2',command=add_data,padx=10,pady=5)
    add.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    # add.place(x=735,y=460)

    addFrame = Frame(top, height=800, width=1920, bg='#080F19')
    editFrame = Frame(top, height=800, width=1920, bg='#080F19')
    displayFrame = Frame(top, height=800, width=1920, bg='#080F19')
    backbtn=Button(addFrame,text="Back",cursor='hand2',command=clearTopFrame,padx=10,pady=5)
    editbackbtn=Button(editFrame,text="Back",cursor='hand2',command=clearEditFrame,padx=10,pady=5)

    loginFrame = Frame(upperFrame, height=800, width=1920, bg='#080F19')

    username_var = StringVar()
    username_lbl = Label(loginFrame, text='Username: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=600, y=150)
    username_ans_lbl = Entry(loginFrame, width=20,state=NORMAL,textvariable = username_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=695, y=150)
    username_line_frame = Frame(loginFrame, width=200,height=2,bg='#91C46B').place(x=695, y=175)

    password_var = StringVar()
    password_lbl = Label(loginFrame, text='Password: ', fg='#fff', bg='#080F19', font=('Raleway', 13)).place(x=600, y=220)
    password_ans_lbl = Entry(loginFrame, width=20,state=NORMAL,show="*", textvariable = password_var, fg='#909394', bg='#080F19',bd=0, font=('Raleway', 12)).place(x=695, y=220)
    password_line_frame = Frame(loginFrame, width=200,height=2,bg='#91C46B').place(x=695, y=245)

    login_submit=Button(loginFrame,text="login",cursor='hand2',command=loginSubmit,padx=9,pady=4)
    login_submit.configure(background='#364156', foreground='white',font=('arial',14))
    login_submit.place(x=745,y=280)

    logout_submit=Button(top,text="logout",cursor='hand2',command=logoutSubmit,padx=9,pady=4)
    logout_submit.configure(background='#364156', foreground='white',font=('arial',14))
    # logout_submit.place(x=10,y=10)

    login_backbtn=Button(loginFrame,text="Back",cursor='hand2',bg='#080F19', fg='#fff',command=login_back,padx=9,pady=4,font=('arial',14)).place(x=655,y=280)

    edit=Button(upperFrame,text="Edit data",cursor='hand2',command=openEditDialog,padx=10,pady=5)
    edit.configure(background='#364156', foreground='white',font=('arial',15,'bold'))
    # edit.place(x=735,y=340)

    editDialogFrame = Frame(upperFrame, height=300, width=500, bg='#14253E')
    dialogAlert_lbl = Label(editDialogFrame, text='Please enter valid number!', fg='red', bg='#14253E', font=('Raleway', 13, 'bold'))
    # dialogAlert_lbl.place(x=150, y=250)

    #making global
    maleOption = Radiobutton(editFrame, background="#080F19", fg="#ccc",activebackground='#91C46B',activeforeground='red', font=('Raleway', 13), text="Male", variable=gendervar, value=1, command=setgenderOption)
    femaleOption = Radiobutton(editFrame, background="#080F19", fg="#ccc",activebackground='#91C46B',activeforeground='red', font=('Raleway', 13), text="Female", variable=gendervar, value=2, command=setgenderOption)
    #------------------

    # photoPath = 'tempimg/'+("1")+'.jpg'
    # user_image = writeToFile(maininfo[14],photoPath)

    # f = Image.open(photoPath)
    # img = f.resize((215,250),Image.ANTIALIAS)
    # userimg = ImageTk.PhotoImage(img)

    # selected_img = Label(top, bg='red',width=215,height=250, image=userimg)
    # selected_img.place(x=1110,y=250)

    # instruction_lbl = Label(top, text='Please Upload image by clicking below button', fg='#333', bg='#CDCDCD', font=('Raleway', 20, 'bold')).place(x=430, y=400)
    
    # setFormUI()  

    top.mainloop()


















