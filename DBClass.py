import mysql.connector
from mysql.connector import Error

class DBClass:

	def __init__(self):
		try:
		    self.conn = mysql.connector.connect(host='localhost',
		                                         database='python_nprs',
		                                         user='yash',
		                                         password='yash2700')
		    if self.conn.is_connected():
		        db_Info = self.conn.get_server_info()
		        print("Connected to MySQL Server version ", db_Info)
		        self.c = self.conn.cursor()
		        # self.c.execute("select database();")
		        # record = self.c.fetchone()
		        # print("You're connected to database: ", record)

		except Error as e:
		    print("Error while connecting to MySQL", e)

	def creating_database(self):
		self.c.execute('''CREATE TABLE IF NOT EXISTS UserInfo(user_id INT auto_increment primary key, name TEXT, dob TEXT,address TEXT, age INT, gender TEXT, email TEXT, contact1 CHAR(11), contact2 CHAR(11), marital_status TEXT, vehicle_type TEXT, vehicle_model TEXT, vehicle_register TEXT, vehicle_number TEXT);''')
		print("Table created successfully!")

	def get_rowCount(self):
		try:
			count = 0
			cur = self.c.execute("SELECT * from userinfo") #storing data in a variable
			crrr = self.c.fetchall()
			for row in crrr:
				count +=1
			return count
		except Exception as e:
			print(e, "exception occured!!")

	def checkCredentials(self, username, password):
		try:
			count = 0
			cur = self.c.execute("SELECT * from admin where username = %s AND password = %s", (username, password,)) #storing data in a variable
			crrr = self.c.fetchall()
			for row in crrr:
				count +=1
			return count
		except Exception as e:
			print(e, "exception occured!!")

	def fetchData(self, number):
		n = str(number)
		print("fetching "+n+", Please wait...")
		try:
			mainlist = []
			data_list = []
			# cur = self.c.execute("SELECT * from userinfo where vehicle_number = %s",(n,)) #storing data in a variable
			cur = self.c.execute("SELECT * from userinfo where vehicle_number = %s",(number,))
			crrr = self.c.fetchall()
			for row in crrr:
				data_list = [row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],row[10],row[11],row[12],row[13]]
			
			print("Operations performed successfully!")
			return data_list
		except Exception as e:
			print(e, "exception occured!!")
			print("No Entry in database!")

	def insert_data(self, name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number):
		try:
			tid = self.get_rowCount()
			# print("count: ",tid)
			tid += 1
			# print("tid: ",tid)
			self.c.execute('''INSERT INTO userinfo VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''',(tid, name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number))
			# self.c.execute('''INSERT INTO userinfo VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',(name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number))
			print("Records entered successfully!")
			self.conn.commit()
		except Exception as e:
			print(e, "exception occured!!")
			print("No database created!")

	def update_data(self, name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number):
		try:
			self.c.execute('''UPDATE userinfo SET name=%s, dob=%s, address=%s, age=%s, gender=%s, email=%s, contact1=%s, contact2=%s, marital_status=%s, vehicle_type=%s, vehicle_model=%s, vehicle_register=%s WHERE vehicle_number = %s''',(name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number))
			# self.c.execute('''INSERT INTO userinfo VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',(name, dob, addr, age, gender, email, contact1, contact2, marital, vehicle_type, vehicle_model, vehicle_register, vehicle_number))
			print("Records entered successfully!")
			self.conn.commit()
		except Exception as e:
			print(e, "exception occured!!")
			print("Update exception!")